# DevOps using GitLab CI/CD, Tearraform, Kaniko, AWS - automating SpringBoot API deployment on ECS

## Architecture Diagram

![Architecture_Diagram](images/Architecture_Diagram.png)

## Getting started

1. Fork/Clone this repo & make sure to create two branches - main and production.
2. Install and configure awscli/terraform with IAM user Access Key ID & Secret Access Key having Administrator Privileges.
3. Generate RSA Keypair (id_rsa.pub & id_rsa) if not already present on home dir (e.g. ~/.ssh/id_rsa.pub).
4. Create basic Infrastructure on AWS required for GitLab CI/CD and Terraform.
5. Create necessary variables on GitLab using output of step 3.
6. Make necessary modifications to Terraform files using output of step 3.
7. Push changes to 'main' branch and monitor GitLab pipeline for Staging Env creation.
8. Test API deployed on Staging. Check staging apply logs to get urls of ALB, Bastion & RDS. Test Bastion SSH Access/PGAdmin console. Proceed to next steps (Production deployment) if everything looks ok on staging. (Optionally you can destroy staging Env manually using Destroy stage).
9. Create a merge request from 'main' branch to 'production' branch. This should trigger the pipeline stages Test and Validate Terraform.
10. If pipeline succeeds from step 9, accept the merge request. This should trigger the entire pipeline including staging apply and production build.
11. If there are no errors on any stage, pipeline would wait for Manual Intervention to deploy to Production. Proceed to Production apply if everything looks ok on staging.
12. Check API deployed on Production. Check Production apply logs to get urls of ALB, Bastion & RDS. (Optionally you can destroy staging and production envs manually using Destroy stage).

![gitlab_pipeline_stages](images/gitlab_pipeline_stages.png)

### Step 1 - Fork/Clone Repo

```bash
git clone {git repo url}
git checkout -b production
git push
git checkout main
# git push
```

### Step 2 - Install & configure awscli/terraform

- [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- [Create IAM Admin user](https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html)
- [Install Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)

```bash
# Keep Admin IAM user access key ID and secret access key ready
aws configure
```

### Step 3 - Generate RSA Keypair

```bash
ssh-keygen -t rsa
```

### Step 4 - Create basic Infrastructure on AWS required for GitLab CI/CD and Terraform

```bash
cd create-basic-infra-aws
terraform init
terraform apply
cat terraform.tfstate | grep secret
```

- Make a note of the output after terraform apply

```bash
# after terraform apply is complete, check log output
ecr-repo-uri = "5751535XXXXX.dkr.ecr.us-east-1.amazonaws.com/gitlab-cicd-ecr"
iamuser-access-key-id = "XXXXXXXXXXX"
tf-backend-bucket-name = "gitlab-cicd-tf-backend-bucket-2022042413350655XXXXXXXX"
tfstate-dynamodb-table-name = "tfstate-dynamodb-table"

# cat terraform.tfstate | grep secret
"secret": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
```

### Step 5 - Create necessary variables on GitLab using step 4 output

- Create variables (repo settings -> CI/CD -> Variables) as shown on the screenshot below.
  ![GitLabVariables_screenshot](images/GitLabVariables_screenshot.png)

### Step 6 - Make necessary modifications to Terraform files using step 4 output

- Modify bucket value on deploy/backend.tf using tf-backend-bucket-name from step 4
- Modify ecr_image_api variable default value on deploy/variables.tf using ecr-repo-uri from step 4. Make sure to have 'latest' tag at the end (This is only necessary if you run terraform manually from deploy dir locally. If you run 'terraform apply' prior to GitLab pipeline job success, you will get an error because there are no container images on ECR. You can choose to build & manually push container image to ECR anytime !).

### Step 7/9 - Push changes to 'main' branch and monitor GitLab pipeline for Staging Env creation

```bash
# push to staging
git commit -am 'updated main branch'
git push
# push to production
git checkout production
git merge main
git push
```

### Step 8/12 - Check/Test Deployed API

- Check pipeline stage log (Staging Apply/Production Apply) to get API endpoint (alb_dns), Bastion (bastion_host) & RDS DNS (db_host).

```
Apply complete! Resources: 48 added, 0 changed, 0 destroyed.
Outputs:
alb_dns = "spring-staging-alb-14913xxxxx.us-east-1.elb.amazonaws.com"
bastion_host = "ec2-44-201-xxx-xxx.compute-1.amazonaws.com"
db_host = "spring-staging-db.xxxxvmxuscag.us-east-1.rds.amazonaws.com"
db_url = "jdbc:postgresql://spring-staging-db.xxxxxxmxuscag.us-east-1.rds.amazonaws.com:5432/springappdb"
```

```bash
curl -i http://{alb_dns}/
```

### API GET Response after initial deployment to Staging/Production

```json
[
  {
    "id": -1,
    "projectName": "DevOps using GitLab CI/CD, Tearraform, Kaniko, AWS - automating SpringBoot API deployment on ECS",
    "projectTools": "GitLab CI/CD, Terraform, SpringBoot, Kaniko, AWS (VPC, EC2, ECS, ECR, ALB, RDS, S3, DynamoDB, IAM, SSM, CloudWatch)",
    "comment": "Current time is 2022-04-24T00:52:01.182838. CRUD API endpoint is '/projects' & you may send a POST request to '/projects' to add a sample project"
  }
]
```

```bash
# POST request to create a sample project
curl -i -X POST -H "Content-Type:application/json" -d '{  "projectName" : "DevOps using GitLab CI/CD, Terraform, AWS", "projectTools" : "aws, terraform", "comment" : "DevOps" }' http://{alb_dns}/projects
# GET request to list projects
curl -i http://{alb_dns}/projects/
# GET request to list a particular project
curl -i http://{alb_dns}/projects/{id}
# PUT request to update a particular project
curl -i -X PUT -H "Content-Type:application/json" -d '{  "projectName" : "DevOps using GitLab CI/CD, Terraform, AWS - automating SpringBoot API deployment on ECS", "projectTools" : "aws, terraform, SpringBoot, Kaniko", "comment" : "DevOps is fun!" }' http://{alb_dns}/projects/{id}
# DELETE request to delete a particular project
curl -i -X DELETE http://{alb_dns}/projects/{id}
```

### SSH to bastion

```bash
# Password less ssh using private key from step 3
ssh ec2-user@{bastion_host}
```

### Access PGAdmin console

- On browser open url : http://{bastion_host}:9090/
- Use credentials (deploy/scripts/bastion/user-data.sh) username: 'random_user@domain.local' & password: 'supersecret911' (Not recommended practice to hardcode credential this way. Use SSM Parameter store/Secret Manager. I am too tired to do this now. LOL !)
- Add postgres db server with credentials (use {db_host} from terraform apply output & Check variables on GitLab TF_VAR_db_username/TF_VAR_db_password)

### Areas to improve

- Remove hardcoded credentials from user-data script (We may not need PGAdmin access through bastion).
- Add Route53 DNS for ALB Endpoint and secure with HTTPS using ACM.
- Add Autoscaling in ECS.
- Remove NATGateway & use PrivateLink.

P.S. Drop a star & share this repo if you found this useful.
