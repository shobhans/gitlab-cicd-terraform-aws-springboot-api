package com.example.springboot.mycoolapp.rest;

import com.example.springboot.mycoolapp.entity.Project;
import com.example.springboot.mycoolapp.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@RestController
public class HelloController {

	@Value("${project.name}")
	private String projectName;

	@Value("${project.tools}")
	private String projectTools;

	@Autowired
	private ProjectRepository projectRepository;

	@GetMapping("/")
	public List<Project> listProjects(){
		Project project = Project.builder()
							.id(-1L)
							.projectName(projectName)
							.projectTools(projectTools)
							.comment("Current time is " + LocalDateTime.now() +". CRUD API endpoint is '/projects' & you may send a POST request to '/projects' to add a sample project")
							.build();
		if (projectRepository.findAll().isEmpty()){
			return Collections.singletonList(project);
		}
		return projectRepository.findAll();
	}

}
