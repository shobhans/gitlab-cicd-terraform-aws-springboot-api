variable "prefix" {
  default = "spring"
}

variable "project" {
  default = "gitlab-cicd-springboot-app"
}

variable "contact" {
  default = "post.shobhan@gmail.com"
}

variable "project_name" {
  description = "SpringBoot app custom property"
  default     = "DevOps using GitLab CI/CD, Tearraform, Kaniko, AWS - automating SpringBoot API deployment on ECS"
}

variable "project_tools" {
  description = "SpringBoot app custom property"
  default     = "GitLab CI/CD, Terraform, SpringBoot, Kaniko, AWS (VPC, EC2, ECS, ECR, ALB, RDS, S3, DynamoDB, IAM, SSM, CloudWatch)"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "deployer-key"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "408345364506.dkr.ecr.us-east-1.amazonaws.com/gitlab-cicd-ecr:latest"
}
