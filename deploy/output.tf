output "db_host" {
  value       = aws_db_instance.rds-instance.address
  description = "RDS address"
}

output "bastion_host" {
  value       = aws_instance.bastion-server.public_dns
  description = "Public DNS name of bastion server"
}

output "db_url" {
  value       = "jdbc:postgresql://${aws_db_instance.rds-instance.address}:5432/${aws_db_instance.rds-instance.name}"
  description = "RDS DB URL for spring app"
}

output "alb_dns" {
  value       = aws_lb.api.dns_name
  description = "ALB DNS address - api endpoint"
}


