resource "aws_db_subnet_group" "rds-subnet-grp" {
  name = "${local.prefix}-rds-subnet-grp"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-rds-subnet-grp" })
  )
}

resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance."
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432
    security_groups = [
      aws_security_group.ecs-service-sg.id,
      aws_security_group.bastion-sg.id
    ]

  }

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-rds-sg" })
  )
}

resource "aws_db_instance" "rds-instance" {
  identifier              = "${local.prefix}-db"
  name                    = "springappdb"
  allocated_storage       = 20
  storage_type            = "gp2"
  engine                  = "postgres"
  engine_version          = "12"
  instance_class          = "db.t2.micro"
  db_subnet_group_name    = aws_db_subnet_group.rds-subnet-grp.name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0
  multi_az                = false
  skip_final_snapshot     = true
  vpc_security_group_ids  = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-rds" })
  )
}
