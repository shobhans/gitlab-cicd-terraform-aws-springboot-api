terraform {
  backend "s3" {
    bucket         = "gitlab-cicd-tf-backend-bucket-20220424194520604200000001"
    key            = "springboot-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "tfstate-dynamodb-table"
  }
}
