resource "aws_ssm_parameter" "project-tools" {
  name  = "/${terraform.workspace}/api/projectTools"
  type  = "String"
  value = var.project_tools
}

resource "aws_ssm_parameter" "db-url" {
  name  = "/${terraform.workspace}/api/dbUrl"
  type  = "SecureString"
  value = "jdbc:postgresql://${aws_db_instance.rds-instance.address}:5432/${aws_db_instance.rds-instance.name}"
}

resource "aws_ssm_parameter" "dbUsername" {
  name  = "/${terraform.workspace}/api/dbUsername"
  type  = "SecureString"
  value = aws_db_instance.rds-instance.username
}

resource "aws_ssm_parameter" "dbPassword" {
  name  = "/${terraform.workspace}/api/dbPassword"
  type  = "SecureString"
  value = aws_db_instance.rds-instance.password
}