[
    {
        "name": "api",
        "image": "${app_image}",
        "essential": true,
        "memoryReservation": 256,
        "environment": [
            {"name": "PROJECT_NAME", "value": "${project_name}"}
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": "${log_group_name}",
                "awslogs-region": "${log_group_region}",
                "awslogs-stream-prefix": "api"
            }
        },
        "portMappings": [
            {
                "containerPort": 8080,
                "hostPort": 8080
            }
        ],
        "secrets": [
            {
                "name": "PROJECT_TOOLS",
                "valueFrom": "${project_tools}"
            },
            {
                "name": "SPRING_DATASOURCE_URL",
                "valueFrom": "${db_url}"
            },
            {
                "name": "SPRING_DATASOURCE_USERNAME",
                "valueFrom": "${db_user}"
            },
            {
                "name": "SPRING_DATASOURCE_PASSWORD",
                "valueFrom": "${db_pass}"
            }
        ]
    }
]
