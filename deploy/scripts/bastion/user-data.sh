#!/bin/bash

sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user
#pgadmin docker container for accessing postgres rds via browser on port https://bastion-dns-address:9090/
#make sure to open port 9090 on bastion server security group
docker run -d -e PGADMIN_DEFAULT_EMAIL=random_user@domain.local -e PGADMIN_DEFAULT_PASSWORD='supersecret911' -p 9090:80 --name=pgadmin dpage/pgadmin4:6
