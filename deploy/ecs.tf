resource "aws_ecs_cluster" "ecs-cluster" {
  name = "${local.prefix}-cluster"

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-ecs-cluster" })
  )
}

resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  description = "Allow retrieving images and adding to logs"
  policy      = file("./scripts/ecs/task-exec-role.json")
}

resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./scripts/ecs/assume-role-policy.json")

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-ecs-task-exec-role" })
  )
}

resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./scripts/ecs/assume-role-policy.json")

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-app-iam-role" })
  )
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-ecs-task-log-grp" })
  )
}

data "template_file" "api_container_definition" {
  template = file("./scripts/ecs/container-definition.json.tpl")

  vars = {
    app_image     = var.ecr_image_api
    project_name  = var.project_name
    project_tools = aws_ssm_parameter.project-tools.arn
    #db_host          = aws_db_instance.rds-instance.address
    #db_name          = aws_db_instance.rds-instance.name
    db_url           = aws_ssm_parameter.db-url.arn
    db_user          = aws_ssm_parameter.dbUsername.arn
    db_pass          = aws_ssm_parameter.dbPassword.arn
    log_group_name   = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region = data.aws_region.current.name
  }
}

resource "aws_ecs_task_definition" "api" {
  family                   = "${local.prefix}-api"
  container_definitions    = data.template_file.api_container_definition.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.task_execution_role.arn
  task_role_arn            = aws_iam_role.app_iam_role.arn

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-ecs-task-def" })
  )
}

resource "aws_ecs_service" "api" {
  name             = "${local.prefix}-api"
  cluster          = aws_ecs_cluster.ecs-cluster.name
  task_definition  = aws_ecs_task_definition.api.family
  desired_count    = 1
  launch_type      = "FARGATE"
  platform_version = "LATEST"

  # network_configuration {
  #   subnets = [
  #     aws_subnet.pulic-subnet-a.id,
  #     aws_subnet.pulic-subnet-b.id
  #   ]
  #   assign_public_ip = true
  # }
  network_configuration {
    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]
    security_groups = [aws_security_group.ecs-service-sg.id]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = "api"
    container_port   = 8080
  }

  #depends_on = [aws_lb_listener.api_https]
  depends_on = [aws_lb_listener.api]

}

resource "aws_security_group" "ecs-service-sg" {
  description = "Access for the ECS service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
  }

  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    security_groups = [aws_security_group.lb.id]
    #cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-ecs-svc-sg" })
  )
}





