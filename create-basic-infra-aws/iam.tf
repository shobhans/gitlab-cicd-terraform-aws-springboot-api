resource "aws_iam_user" "cicd-user" {
  name = "gitlab-cicd-user"

}

resource "aws_iam_access_key" "cicd-user-access-key" {
  user = aws_iam_user.cicd-user.name
}

resource "aws_iam_policy" "cicd-user-iam-policy" {
  name        = "cicd-user-iam-policy"
  description = "policy for gitlab cicd user for deployment"
  policy      = file("cicd_user_iam_policy.json")

}

resource "aws_iam_user_policy_attachment" "iam-policy-attach" {
  user       = aws_iam_user.cicd-user.name
  policy_arn = aws_iam_policy.cicd-user-iam-policy.arn
}

output "iamuser-secret-access-key" {
  value     = aws_iam_access_key.cicd-user-access-key.encrypted_secret
  sensitive = true
}

output "iamuser-access-key-id" {
  value = aws_iam_access_key.cicd-user-access-key.id
}
