variable "prefix" {
  default = "spring"
}

variable "project" {
  default = "gitlab-cicd-springboot-app"
}

variable "contact" {
  default = "post.shobhan@gmail.com"
}

