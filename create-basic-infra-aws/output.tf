output "tf-backend-bucket-name" {
  value       = aws_s3_bucket.tf-backend-bucket.bucket
  description = "Terraform s3 backend bucket name."
}

output "tfstate-dynamodb-table-name" {
  value       = aws_dynamodb_table.tfstate-lock-dynamodb-table.name
  description = "Terraform tfstate lock dynamodb table name."
}

output "ecr-repo-uri" {
  value       = aws_ecr_repository.ecr-repo.repository_url
  description = "ECR URI"
}