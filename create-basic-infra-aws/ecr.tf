resource "aws_ecr_repository" "ecr-repo" {
  name                 = "gitlab-cicd-ecr"
  image_tag_mutability = "MUTABLE"
  encryption_configuration {
    encryption_type = "AES256"
  }

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-ecr-repo" })
  )
}